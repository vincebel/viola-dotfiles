cp -r ../.config .
cp -r ../.bash* .
cp -r ../.xinitrc .

rm -rf .config/discord .config/libreoffice ./config/spotify ./config/wireshark

echo "Backed up\n"
